package com.dfhz.ken.mypubapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.btn_weixin)
    Button btnWeixin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_weixin)
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_weixin:
                Intent intent = new Intent(MainActivity.this,PullRefrshActivity.class);
                startActivity(intent);
                break;
        }
    }
}
