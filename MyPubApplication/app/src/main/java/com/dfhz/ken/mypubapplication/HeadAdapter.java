package com.dfhz.ken.mypubapplication;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Renny on 2018/1/3.
 */

public class HeadAdapter extends CommonAdapter<String> {
    private int layoutId;
    public HeadAdapter( int layoutId, List<String> datas) {
        super( layoutId, datas);
        this.layoutId = layoutId;
    }


    @Override
    protected void convert(ViewHolder holder, final int position) {
        String data = getData(position);
        TextView tv;
        ImageView img = null;
        tv = holder.getView(R.id.item_title);
        img = holder.getView(R.id.imgHeader);
        tv.setText(data);
        switch (position) {
            case 0:
                img.setImageResource(R.drawable.recent2x);
                break;
            case 1:
                img.setImageResource(R.drawable.transfer2x);
                break;
            case 2:
                img.setImageResource(R.drawable.adapt2x);
                break;
            case 3:
                img.setImageResource(R.drawable.security2x);
                break;
            default:
                break;

        }
        if (mItemClickListener != null) {
            if (img != null) {
                img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.onItemClicked(position, v);
                    }
                });
            }
        }
    }


}

