package com.dfhz.ken.mypubapplication;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.dfhz.ken.mypubapplication.widget.PullToRefrshLinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Author:Ken
 * Data：2018/12/5-10:32
 * Todo: doit
 */

public class PullRefrshActivity extends AppCompatActivity {

    @Bind(R.id.lin_refresh)
    PullToRefrshLinearLayout linRefresh;
    Handler handler = new Handler();


    MyRecycleView recyclerView;

    List<String> datas = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pullrefresh);
        recyclerView = findViewById(R.id.listview);
        ButterKnife.bind(this);
        init();
        // initEvent();
    }

    private void init(){
        for(int i = 0; i < 100; i++){
            datas.add("File" + i);
        }

        ExtendHeadAdapter extendHeadAdapter = new ExtendHeadAdapter(R.layout.adapter_item_view, datas);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3,
                GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(extendHeadAdapter);
    }

    private void initEvent(){
//        linRefresh.hideFooterView();
        linRefresh.setOnHeaderRefreshListener(new PullToRefrshLinearLayout.OnHeaderRefreshListener() {
            @Override
            public void onHeaderRefresh(PullToRefrshLinearLayout view) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        linRefresh.onHeaderRefreshComplete();
//                        linRefresh.showFooterView();
                    }
                },2000);
            }
        });

        linRefresh.setOnFooterRefreshListener(new PullToRefrshLinearLayout.OnFooterRefreshListener() {
            @Override
            public void onFooterRefresh(PullToRefrshLinearLayout view) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        linRefresh.onFooterRefreshComplete();
                    }
                },2000);
            }
        });
    }


}
