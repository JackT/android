package com.dfhz.ken.mypubapplication;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;



import java.util.List;

/**
 * Created by Renny on 2018/1/3.
 */

public class ExtendHeadAdapter extends CommonAdapter<String> {
    private int layoutId;
    public ExtendHeadAdapter( int layoutId, List<String> datas) {
        super( layoutId, datas);
        this.layoutId = layoutId;
    }


    @Override
    protected void convert(ViewHolder holder, final int position) {
        String data = getData(position);
        EditText tv;
        tv = holder.getView(R.id.fileName);
        tv.setText(data);

    }


}

